#ifndef DS1302MODULE_H
#define DS1302MODULE_H

#include <Arduino.h>

class DS1302Module {
	private:
		static String toShortName(String);
		static String padZero(String, byte);
		static const unsigned int DELAY_MICROSECONDS = 50;
		static const byte SECONDS = 0x80;
		static const byte MINUTES = 0x82;
		static const byte HOURS = 0x84;
		static const byte DAY = 0x86;
		static const byte MONTH = 0x88;
		static const byte WEEKDAY = 0x8A;
		static const byte YEAR = 0x8C;
		static const byte PROTECTION = 0x8E;
		static const byte TCS = 0x90;
		static const String MONTH_NAMES[];
		static const String WEEKDAY_NAMES[];
		byte rstPin;
		byte clkPin;
		byte datPin;
		byte Dec2BCD(byte);
		byte BCD2Dec(byte);
		void setPin(byte, bool);
		byte getPin(byte);
		void writeByte(byte);
		byte readByte();
		void setData(byte, byte);
		byte getData(byte);
	public:
		static String toMonthLongName(byte);
		static String toMonthShortName(byte);
		static String toWeekdayLongName(byte);
		static String toWeekdayShortName(byte);
		static String toISO8601Date(byte, byte, byte);
		static String toISO8601Time(byte, byte, byte);
		static String toISO8601DateTime(byte, byte, byte, byte, byte, byte);
		DS1302Module(byte, byte, byte);
		void initial();
		void setSeconds(byte);
		void setMinutes(byte);
		void setHours(byte);
		void setDay(byte);
		void setMonth(byte);
		void setWeekday(byte);
		void setYear(byte);
		void setProtection(bool);
		void setRAM(byte, byte);
		void setText(String);
		byte getSeconds();
		byte getMinutes();
		byte getHours();
		byte getDay();
		byte getMonth();
		byte getWeekday();
		byte getYear();
		bool isProtection();
		byte getRAM(byte);
		String getText();
};

#endif