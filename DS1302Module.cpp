#include <DS1302Module.h>

String DS1302Module::toShortName(String text) {
	return text.substring(0, 3);
}

String DS1302Module::padZero(String text, byte length) {
	while (text.length() < length) {
		text = "0" + text;
	}
	return text;
}

byte DS1302Module::Dec2BCD(byte data) {
	return ((data / 10) << 4) + (data % 10);
}

byte DS1302Module::BCD2Dec(byte data) {
	return ((data & 0xF0) >> 4) * 10 + (data & 0x0F);
}

void DS1302Module::setPin(byte pin, bool data) {
	pinMode(pin, OUTPUT);
	digitalWrite(pin, data);
	delayMicroseconds(DS1302Module::DELAY_MICROSECONDS);
}

byte DS1302Module::getPin(byte pin) {
	pinMode(pin, INPUT);
	byte data = digitalRead(pin);
	delayMicroseconds(DS1302Module::DELAY_MICROSECONDS);
	return data;
}

void DS1302Module::writeByte(byte data) {
	for (byte shift = 0x01; shift > 0; shift <<= 1) {
		this->setPin(this->clkPin, LOW);
		this->setPin(this->datPin, (data & shift) > 0);
		this->setPin(this->clkPin, HIGH);
	}
}

byte DS1302Module::readByte() {
	byte data = 0;
	for (byte shift = 0x01; shift > 0; shift <<= 1) {
		this->setPin(this->clkPin, LOW);
		if (this->getPin(this->datPin)) {
			data |= shift;
		}
		this->setPin(this->clkPin, HIGH);
	}
	return data;
}

void DS1302Module::setData(byte address, byte data) {
	this->setPin(this->rstPin, HIGH);
	this->writeByte(address);
	this->writeByte(this->Dec2BCD(data));
	this->setPin(this->rstPin, LOW);
}

byte DS1302Module::getData(byte address) {
	this->setPin(this->rstPin, HIGH);
	this->writeByte(address | 0x01);
	byte data = readByte();
	this->setPin(this->rstPin, LOW);
	return this->BCD2Dec(data);
}

String DS1302Module::toMonthLongName(byte data) {
	const String NAMES[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	return NAMES[(data % 12) + 1];
}

String DS1302Module::toMonthShortName(byte data) {
	return DS1302Module::toShortName(DS1302Module::toMonthLongName(data));
}

String DS1302Module::toWeekdayLongName(byte data) {
	const String NAMES[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
	return NAMES[(data % 7)];
}

String DS1302Module::toWeekdayShortName(byte data) {
	return DS1302Module::toShortName(DS1302Module::toWeekdayLongName(data));
}

String DS1302Module::toISO8601Date(byte year, byte month, byte day) {
	String text = "";
	text += DS1302Module::padZero(String(year), 2);
	text += "-";
	text += DS1302Module::padZero(String(month), 2);
	text += "-";
	text += DS1302Module::padZero(String(day), 2);
	return text;
}

String DS1302Module::toISO8601Time(byte hours, byte minutes, byte seconds) {
	String text = "";
	text += DS1302Module::padZero(String(hours), 2);
	text += ":";
	text += DS1302Module::padZero(String(minutes), 2);
	text += ":";
	text += DS1302Module::padZero(String(seconds), 2);
	return text;
}

String DS1302Module::toISO8601DateTime(byte year, byte month, byte day, byte hours, byte minutes, byte seconds) {
	return DS1302Module::toISO8601Date(year, month, day) + " " + DS1302Module::toISO8601Time(hours, minutes, seconds);
}

DS1302Module::DS1302Module(byte rstPin, byte clkPin, byte datPin) {
	this->rstPin = rstPin;
	this->clkPin = clkPin;
	this->datPin = datPin;
}

void DS1302Module::initial() {
	this->setPin(this->rstPin, LOW);
	this->setPin(this->clkPin, LOW);
}

void DS1302Module::setSeconds(byte data) {
	this->setData(DS1302Module::SECONDS, data % 60);
}

void DS1302Module::setMinutes(byte data) {
	this->setData(DS1302Module::MINUTES, data % 60);
}

void DS1302Module::setHours(byte data) {
	this->setData(DS1302Module::HOURS, data % 24);
}

void DS1302Module::setDay(byte data) {
	this->setData(DS1302Module::DAY, ((data - 1) % 31) + 1);
}

void DS1302Module::setMonth(byte data) {
	this->setData(DS1302Module::MONTH, ((data - 1) % 12) + 1);
}

void DS1302Module::setWeekday(byte data) {
	this->setData(DS1302Module::WEEKDAY, data % 7);
}

void DS1302Module::setYear(byte data) {
	this->setData(DS1302Module::YEAR, data % 100);
}

void DS1302Module::setProtection(bool data) {
	this->setData(DS1302Module::PROTECTION, data ? 0x80 : 0x00);
}

void DS1302Module::setRAM(byte address, byte data) {
	if (address > 30) {
		address = 30;
	}
	this->setData(0xC0 | (address << 1), data);
}

void DS1302Module::setText(String text) {
	const byte LENGTH = min(text.length(), 30);
	this->setRAM(30, LENGTH);
	for (byte i = 0; i < LENGTH; i++) {
		this->setRAM(i, text.charAt(i));
	}
}

byte DS1302Module::getSeconds() {
	return this->getData(DS1302Module::SECONDS);
}

byte DS1302Module::getMinutes() {
	return this->getData(DS1302Module::MINUTES);
}

byte DS1302Module::getHours() {
	return this->getData(DS1302Module::HOURS);
}

byte DS1302Module::getDay() {
	return this->getData(DS1302Module::DAY);
}

byte DS1302Module::getMonth() {
	return this->getData(DS1302Module::MONTH);
}

byte DS1302Module::getWeekday() {
	return this->getData(DS1302Module::WEEKDAY) % 7;
}

byte DS1302Module::getYear() {
	return this->getData(DS1302Module::YEAR);
}

bool DS1302Module::isProtection() {
	return (this->getData(DS1302Module::PROTECTION) & 0x80) > 0;
}

byte DS1302Module::getRAM(byte address) {
	if (address > 30) {
		address = 30;
	}
	return this->getData(0xC0 | (address << 1));
}

String DS1302Module::getText() {
	String text = "";
	const byte LENGTH = this->getRAM(30);
	for (byte i = 0; i < LENGTH; i++) {
		text += ((char) this->getRAM(i));
	}
	return text;
}